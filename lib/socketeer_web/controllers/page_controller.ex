defmodule SocketeerWeb.PageController do
  use SocketeerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
