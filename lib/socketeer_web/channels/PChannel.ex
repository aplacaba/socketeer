defmodule SocketeerWeb.PChannel do
  use Phoenix.Channel

  def join("services:pose", _message, socket) do
    {:ok, socket}
  end

  def handle_in("pose_data", payload, socket) do
    publish(Jason.encode!(payload))
    {:reply, :ok, socket}
  end

  def handle_in("get_settings", payload, socket) do
    settings = load_configuration_from_file()
    {:reply, {:ok, Map.get(settings, "pose_settings")}, socket}
  end

  defp publish(data) do
    Socketeer.Queue.publish(data)
  end

  defp load_configuration_from_file do
    case File.read(Path.join(:code.priv_dir(:socketeer), "data/config.json")) do
      {:ok, config} -> Jason.decode!(config)
      {:error, _msg} -> %{}
    end
  end
end
