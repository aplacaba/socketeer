defmodule Socketeer do
  @moduledoc """
  Socketeer keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def start_pose do
    SocketeerWeb.Endpoint.broadcast "services:pose", "start", %{}
  end
end
