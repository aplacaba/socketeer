defmodule Socketeer.Queue do
  use GenServer
  use AMQP

  @name __MODULE__
  @queue "pose_data"
  @exchange "pose_exchange"
  @host "amqp://guest@localhost"

  def start_link(args \\ {}) do
    GenServer.start_link(@name, [], name: @name)
  end

  def publish(data) do
    GenServer.cast(@name, {:data, data})
  end

  def init(_args) do
    # setup channel
    {:ok, conn} = AMQP.Connection.open(@host)
    {:ok, chan} = AMQP.Channel.open(conn)
    Exchange.declare(chan, @exchange)
    Queue.bind(chan, @queue, @exchange)
    {:ok, chan}
  end

  def handle_cast({:data, data}, channel) do
    AMQP.Basic.publish(channel, @exchange, "", Jason.encode!(data),
      content_type: "text/plain",
      content_encoding: "utf8")
    {:noreply, channel}
  end
end
